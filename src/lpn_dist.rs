use std::{
    fmt::{Debug, Display},
    ops::{Add, AddAssign, BitXor, Mul, MulAssign, Sub, SubAssign},
};

use nalgebra::base::DVector;
use num_traits::identities::{One, Zero};
use rand::{distributions::Bernoulli, Rng};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Binary(bool);

impl Binary {
    pub const fn new(b: bool) -> Self {
        Self(b)
    }
}

impl Add for Binary {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0.bitxor(rhs.0))
    }
}

impl AddAssign for Binary {
    fn add_assign(&mut self, rhs: Self) {
        self.0 = self.0.bitxor(rhs.0)
    }
}

impl Sub for Binary {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        self + rhs
    }
}

impl SubAssign for Binary {
    fn sub_assign(&mut self, rhs: Self) {
        *self += rhs;
    }
}

impl Mul for Binary {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self::Output {
        Self(self.0 && rhs.0)
    }
}

impl MulAssign for Binary {
    fn mul_assign(&mut self, rhs: Self) {
        self.0 = self.0 && rhs.0
    }
}

impl Zero for Binary {
    fn zero() -> Self {
        Self(false)
    }

    fn is_zero(&self) -> bool {
        !self.0
    }
}

impl One for Binary {
    fn one() -> Self {
        Self(true)
    }

    fn is_one(&self) -> bool
    where
        Self: PartialEq,
    {
        self.0
    }
}

impl From<bool> for Binary {
    fn from(b: bool) -> Self {
        Self(b)
    }
}

impl From<Binary> for bool {
    fn from(b: Binary) -> Self {
        b.0
    }
}

impl Debug for Binary {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", if self.0 { 1 } else { 0 })
    }
}

impl Display for Binary {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self, f)
    }
}

/// Note that a given implementation might draw noise from pseudorandomness instead of actual randomness, which would technically not be distributed according to the LPN distribution, but allows for more dynamic usage of the trait.
pub trait LPNOracle {
    /// Outputs a sample of the LPN distribution.
    fn next_sample(&mut self) -> (DVector<Binary>, Binary);

    /// Returns the dimension of the `s` vector.
    fn dim(&self) -> usize;
}

/// The LPN distribution given some `s` and randomness of some variety.
pub struct LPNDist<R: Rng> {
    s: DVector<Binary>,
    rng: R,
    err_dist: Bernoulli,
}

impl<R: Rng> LPNDist<R> {
    /// Creates a new [`LPNDist`] from the given secret and [`Rng`](rand::Rng).
    /// An `error_rate` of `0.0` will result in no error in the output.
    pub fn new(s: DVector<Binary>, rng: R, error_rate: f64) -> Self {
        Self {
            s,
            rng,
            err_dist: Bernoulli::new(error_rate).unwrap(),
        }
    }

    fn rand_vec(&mut self) -> DVector<Binary> {
        let bool_dist = Bernoulli::new(0.5).unwrap();
        let avec: Vec<Binary> = (&mut self.rng)
            .sample_iter(bool_dist)
            .take(self.s.nrows())
            .map(Binary)
            .collect();
        DVector::from(avec)
    }

    fn rand_err(&mut self) -> Binary {
        Binary(self.rng.sample(self.err_dist))
    }
}

impl<R: Rng> LPNOracle for LPNDist<R> {
    fn next_sample(&mut self) -> (DVector<Binary>, Binary) {
        let a = self.rand_vec();
        let e = self.rand_err();
        let v = a.dot(&self.s) + e;
        (a, v)
    }

    fn dim(&self) -> usize {
        self.s.nrows()
    }
}


/// Saves the previously polled values in the oracle so that they might be used in the stream again.
pub struct LPNDataSaver<O: LPNOracle> {
    oracle: O,
    saved: Vec<(DVector<Binary>, Binary)>,
    index: usize,
}

impl<O: LPNOracle> LPNOracle for LPNDataSaver<O> {
    fn next_sample(&mut self) -> (DVector<Binary>, Binary) {
        if self.index < self.saved.len() {
            let v = self.saved[self.index].clone();
            self.index += 1;
            v
        } else {
            let v = self.oracle.next_sample();
            self.saved.push(v.clone());
            self.index += 1;
            v
        }
    }

    fn dim(&self) -> usize {
        self.oracle.dim()
    }
}

impl<O: LPNOracle> LPNDataSaver<O> {
    /// Creates a new instance of [`LPNDataSaver`](Self).
    pub fn new(oracle: O) -> Self {
        Self {
            oracle,
            saved: Vec::new(),
            index: 0,
        }
    }

    /// Starts the oracle over again from the beginning.
    pub fn reset_data_use(&mut self) {
        self.index = 0;
    }

    pub fn num_saved(&self) -> usize {
        self.saved.len()
    }
}

/// Changes the given LPNOracle to output vectors of length `self.0` by filling the empty space in `a` with `0`s.
pub struct LPNOracleAdapter<O: LPNOracle>(pub usize, pub O);

impl<O: LPNOracle> LPNOracle for LPNOracleAdapter<O> {
    fn next_sample(&mut self) -> (DVector<Binary>, Binary) {
        let (a, v) = self.1.next_sample();
        (a.resize_vertically(self.0, Binary::zero()), v)
    }

    fn dim(&self) -> usize {
        self.0
    }
}