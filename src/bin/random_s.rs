use std::time::Instant;

use lpn_solver::{
    bkw::bkw,
    lpn_dist::{Binary, LPNDist}, wht_bkw::wht_bkw,
};
use nalgebra::DVector;
use rand::{thread_rng, Rng};
use structopt::StructOpt;
use colored::Colorize;

/// Command line input arguments.
#[derive(Debug, StructOpt, Clone, Copy)]
#[structopt(
    name = "random s",
    about = "Tests the BKW algorithm against random secrets."
)]
struct Opt {
    /// Length of the secret to randomly generate and find.
    #[structopt(short, long, default_value = "16")]
    slen: usize,

    /// Rate that bits are flipped.
    #[structopt(short, long, default_value = "0.1")]
    error_rate: f64,

    /// Theshhold difference between majority and minority guess to stop.
    #[structopt(short, long, default_value = "10")]
    threshhold: usize,

    /// Number of sections to split the vectors into.
    /// Default set to 2*lg k.
    #[structopt(short)]
    a: Option<usize>,

    /// Number of elements in a batch.
    #[structopt(short)]
    n: Option<usize>,

    /// Whether to use the extended version or not.
    #[structopt(short = "x", long)]
    extention: bool,
}

pub fn main() {
    let opt = Opt::from_args();

    let mut rng = thread_rng();
    let bvec = (0..opt.slen)
        .map(|_| Binary::new(rng.gen_bool(0.5)))
        .collect::<Vec<Binary>>();
    println!("Initiallizing with Following:");
    let s = DVector::from_vec(bvec);
    println!("e: {}", opt.error_rate);
    println!("s: {}", s.transpose());

    let o = LPNDist::new(s.clone(), rng, opt.error_rate);

    println!("Starting Algorithm:");


    let start = Instant::now();
    let (sg, num) = if opt.extention {
        wht_bkw(opt.a, opt.n, opt.threshhold, o)
    } else {
        bkw(opt.a, opt.n, opt.threshhold, o)
    };
    let time = start.elapsed();

    println!("\nResults:");
    println!("s:{}guess:{}difference:{}", s.transpose(), sg.transpose(), (s.clone() + &sg).transpose());
    println!(
        "The vectors are {}!",
        if s == sg { "the same".green() } else { "different".red() }
    );
    println!("Number of samples polled: {}", num);
    println!("Running time:             {}ms", time.as_millis());
}
