use std::{cmp::max, io::{self, Write}};

use nalgebra::DVector;
use num_traits::{One, Zero};

use crate::lpn_dist::{Binary, LPNOracle, LPNOracleAdapter, LPNDataSaver};

/// Does a single BKW reduction step, which reduces the vector lengths by `b` and forms a set of new vectors and values from the vector xors.
fn reduce_step(b: usize, set: &Vec<(DVector<Binary>, Binary)>) -> Vec<(DVector<Binary>, Binary)> {
    if set.is_empty() {
        return Vec::new();
    }
    let d = set[0].0.nrows();
    assert!(d > b);
    let mut sort_vec: Vec<Vec<(DVector<Binary>, Binary)>> = vec![vec![]; 2usize.pow(b as u32)];
    for (a, v) in set {
        let count = &a.as_slice()[d - b..d];
        let mut index = 0;
        let mut mult = 1;
        for bit in count {
            index += if (*bit).into() { 1 } else { 0 } * mult;
            mult *= 2;
        }
        sort_vec[index].push((
            (a.clone()).resize_vertically(d - b, Binary::zero()),
            v.clone(),
        ));
    }

    let mut out_vec = Vec::new();

    for (a, v) in &sort_vec[0] {
        out_vec.push((a.clone(), v.clone()));
    }

    for vec in sort_vec[1..].iter() {
        if !vec.is_empty() {
            let (ax, vx) = vec[0].clone();
            for (a, v) in vec {
                out_vec.push((a + &ax, vx + *v));
            }
        }
    }

    out_vec
}

/// Returns an iterator of samples from the included oracle.
fn get_samples_iter<'a>(
    n: usize,
    o: &'a mut impl LPNOracle,
) -> impl Iterator<Item = (DVector<Binary>, Binary)> + 'a {
    (0..n).into_iter().map(|_| o.next_sample())
}

/// Returns true when every index has a majority guess that is above the threshhold.
fn have_guesses(t: usize, indices: &Vec<usize>, guesses: &Vec<(usize, usize)>) -> bool {
    for i in indices {
        let guess = guesses[*i];
        let abs_diff = if guess.0 > guess.1 {
            guess.0 - guess.1
        } else {
            guess.1 - guess.0
        };
        if abs_diff < t {
            return false;
        }
    }
    return true;
}

/// Returns the index of the 1 that the vector is (i.e. `e_i` vectors return `i`).
/// Returns [`None`] when the input is not a singleton vector.
fn get_singleton_index(vec: DVector<Binary>) -> Option<usize> {
    let mut index = None;
    for i in 0..vec.nrows() {
        if vec[(i, 0)].into() {
            if index.is_some() {
                return None;
            }
            index = Some(i)
        }
    }
    index
}

/// Finds guesses for the value of the `s` vector at the indices.
fn reduce(
    a: usize,
    b: usize,
    n: usize,
    t: usize,
    o: &mut impl LPNOracle,
    indices: &Vec<usize>,
    guesses: &mut Vec<(usize, usize)>,
    step: usize,
) {
    assert!(indices.len() <= b);
    assert!(n >= a * (2usize.pow(b as u32) - 1));
    let mut indices = indices.clone();
    indices.sort();
    let mut sub_step = 1;
    while !have_guesses(t, &indices, guesses) {
        print!("\r\x1B[2KStep: {}-{} / {}", step, sub_step, a);
        io::stdout().flush().unwrap();
        sub_step += 1;
        let mut samples = Vec::new(); // samples with all the rows in `indices` at the top
        for (mut a, v) in get_samples_iter(n, o) {
            let mut i = 0;
            for r in &indices {
                a.swap_rows(i, *r); // puts all the indices of importants at the top
                i += 1;
            }
            samples.push((a, v));
        }

        for _ in 0..(a - 1) {
            samples = reduce_step(b, &samples);
        }

        for (a, v) in samples {
            // Extract the singleton vectors and give the value to the corresponding guess
            if let Some(i) = get_singleton_index(a) {
                if i < indices.len() {
                    if v.into() {
                        guesses[indices[i]].1 += 1;
                    } else {
                        guesses[indices[i]].0 += 1;
                    }
                }
            }
        }
    }
}

/// Runs the BKW algorithm on the given [`LPNOracle`](crate::lpn_dist::LPNOracle) and returns with high probability the vector `s`.
/// If `a` or `n` are [`None`](Option), then they are filled in automatically according to the original specifications.
/// The input `t` is the tolerance between the major and minor vote for a given index for the algorithm to be satisfied.
pub fn bkw(
    a: Option<usize>,
    n: Option<usize>,
    t: usize,
    o: impl LPNOracle,
) -> (DVector<Binary>, usize) {
    let k = o.dim();

    let da = max(1, (2.0 * (k as f64).log2()).ceil() as usize); // Suggested a value from paper, multiplied by a different constant for faster results given smaller k.
    let a = a.unwrap_or(da);
    let b = (k + a - 1) / a; // b = ceil(k/a)

    let dn: usize = max(1, (t + a) * 2usize.pow(b as u32) - a); // Usually there will be t vectors for each index at the end since t * 2^b vectors will make it to the end at least.
    let n = n.unwrap_or(dn);

    println!("k: {}", k);
    println!("a: {}", a);
    println!("b: {}", b);
    println!("n: {}", n);

    let mut o = LPNDataSaver::new(LPNOracleAdapter(a * b, o));

    // Use the reduce function to get all the indices.

    let mut guesses = vec![(0, 0); k];

    for i in 0..a {
        print!("\r\x1B[2KStep: {} / {}", i + 1, a);
        io::stdout().flush().unwrap();
        let mut indices = vec![];
        for j in i * b..i * b + b {
            if j < k {
                indices.push(j);
            }
        }
        o.reset_data_use();
        reduce(a, b, n, t, &mut o, &indices, &mut guesses, i + 1);
    }

    let vectorized = guesses
        .iter()
        .map(|(z, o)| if z > o { Binary::zero() } else { Binary::one() })
        .collect::<Vec<_>>();

    println!("\rBKW algorithm finished");

    (DVector::from(vectorized), o.num_saved())
}

#[cfg(test)]
mod test {

    use crate::lpn_dist::LPNDist;

    use super::*;

    use rand::prelude::*;
    use rand_pcg::Pcg64;

    const ZE: Binary = Binary::new(false);
    const ON: Binary = Binary::new(true);

    const SEED: u64 = 0xEDAC_5FBE_96C1_B5AB;

    fn create_rng() -> impl Rng {
        Pcg64::seed_from_u64(SEED)
    }

    /// Checks whether [`get_singleton_index`](get_singleton_index) works properly for positive dimension vectors.
    #[test]
    fn singletons_positives() {
        assert_eq!(
            None,
            get_singleton_index(DVector::from_vec(vec![ZE, ZE, ZE, ZE]))
        );
        assert_eq!(
            None,
            get_singleton_index(DVector::from_vec(vec![ON, ZE, ON]))
        );
        assert_eq!(
            Some(2),
            get_singleton_index(DVector::from_vec(vec![ZE, ZE, ON, ZE]))
        );
        assert_eq!(
            Some(3),
            get_singleton_index(DVector::from_vec(vec![ZE, ZE, ZE, ON]))
        );
    }

    /// Checks whether [`get_singleton_index`](get_singleton_index) returns [`None`] for zero dimension vectors.
    #[test]
    fn singletons_zero() {
        assert_eq!(None, get_singleton_index(DVector::from_vec(vec![])));
    }

    /// Checks that the algorithm works given no error.
    /// It should work all of the time, even though it could technically run forever.
    #[test]
    fn no_error_bkw() {
        let rng = create_rng();
        let s = DVector::from_vec(vec![
            ZE, ZE, ON, ON, ZE, ON, ON, ZE, ON, ZE, ON, ON, ZE, ON, ZE, ON, ON, ZE, ZE, ON, ON, ON,
            ON, ZE, ZE, ZE, ON,
        ]);
        let dist = LPNDist::new(s.clone(), rng, 0.0);
        let (sf, samples) = bkw(Some(16), None, 10, dist);
        println!("Number of samples used: {}", samples);
        assert_eq!(s, sf);
    }
}
