use std::{cmp::max, io::{self, Write}};

use nalgebra::DVector;
use num_traits::{One, Zero};

use crate::lpn_dist::{Binary, LPNOracle, LPNOracleAdapter, LPNDataSaver};

/// Does a single BKW reduction step, which reduces the vector lengths by `b` and forms a set of new vectors and values from the vector xors.
fn reduce_step(b: usize, set: &Vec<(DVector<Binary>, Binary)>) -> Vec<(DVector<Binary>, Binary)> {
    if set.is_empty() {
        return Vec::new();
    }
    let d = set[0].0.nrows();
    assert!(d > b);
    let mut sort_vec: Vec<Vec<(DVector<Binary>, Binary)>> = vec![vec![]; 2usize.pow(b as u32)];
    for (a, v) in set {
        let count = &a.as_slice()[d - b..d];
        let mut index = 0;
        let mut mult = 1;
        for bit in count {
            index += if (*bit).into() { 1 } else { 0 } * mult;
            mult *= 2;
        }
        sort_vec[index].push((
            (a.clone()).resize_vertically(d - b, Binary::zero()),
            v.clone(),
        ));
    }

    let mut out_vec = Vec::new();

    for (a, v) in &sort_vec[0] {
        out_vec.push((a.clone(), v.clone()));
    }

    for vec in sort_vec[1..].iter() {
        if !vec.is_empty() {
            let (ax, vx) = vec[0].clone();
            for (a, v) in vec {
                out_vec.push((a + &ax, vx + *v));
            }
        }
    }

    out_vec
}

/// Returns an iterator of samples from the included oracle.
fn get_samples_iter<'a>(
    n: usize,
    o: &'a mut impl LPNOracle,
) -> impl Iterator<Item = (DVector<Binary>, Binary)> + 'a {
    (0..n).into_iter().map(|_| o.next_sample())
}

/// In-place fast Walsh-Hadamard transform from Wikipedia (original in Python).
/// It essentually counts the vectors for which `v` dot `x` for all `x` will result in the same output.
/// Runs in time `b*2^b` in this instance.
fn walsh_hadamard_transform(counts: Vec<isize>, diffs: &mut Vec<usize>) -> (usize, usize) {
    let mut counts = counts;
    let mut h = 1;

    while h < counts.len() {
        for i in (0..counts.len()).step_by(h * 2) {
            for j in i..i + h {
                let x = counts[j];
                let y = counts[j + h];
                counts[j] = x + y;
                counts[j + h] = x - y;
            }
        }
        h *= 2;
    }

    let mut max_ind = 0;
    let mut max = 0;
    let mut sec_max = 0;
    for i in 0..counts.len() {
        diffs[i] += counts[i].abs() as usize;
        let nd = diffs[i];
        if nd > max {
            max_ind = i;
            sec_max = max;
            max = nd;
        } else if nd > sec_max {
            sec_max = nd;
        }
    }

    (max - sec_max, max_ind)
}

/// Guesses the small b-length part of the s-block.
fn reduce(
    a: usize,
    b: usize,
    n: usize,
    t: usize,
    o: &mut impl LPNOracle,
    indices: &Vec<usize>,
    step: usize,
) -> Vec<Binary> {
    assert!(indices.len() <= b);
    let b_enum = 2usize.pow(b as u32);
    assert!(n >= a * (b_enum - 1));
    let mut indices = indices.clone();
    indices.sort();
    let mut sub_step = 1;

    let mut diffs = vec![0usize; b_enum];
    let mut diff_val = 0usize;
    let mut diff_ind = 0usize;

    while diff_val < t {
        print!("\r\x1B[2KStep: {}-{} / {}", step, sub_step, a);
        io::stdout().flush().unwrap();
        sub_step += 1;
        let mut samples = Vec::new(); // samples with all the rows in `indices` at the top
        for (mut a, v) in get_samples_iter(n, o) {
            let mut i = 0;
            for r in &indices {
                a.swap_rows(i, *r); // puts all the indices of importants at the top
                i += 1;
            }
            samples.push((a, v));
        }

        for _ in 0..(a - 1) {
            samples = reduce_step(b, &samples);
        }

        let mut counts = vec![0isize; b_enum];

        for (a, v) in samples {
            let mut mult = 1;
            let mut index = 0;
            for bit in a.as_slice() {
                index += if (*bit).into() { 1 } else { 0 } * mult;
                mult *= 2;
            }
            if v.into() {
                counts[index] += -1;
            } else {
                counts[index] += 1;
            }
        }

        let res = walsh_hadamard_transform(counts, &mut diffs);
        diff_val = res.0;
        diff_ind = res.1;
    }

    let mut guess = vec![];
    let mut btest = b_enum / 2;
    while btest > 0 {
        if diff_ind >= btest {
            diff_ind -= btest;
            guess.push(Binary::one());
        } else {
            guess.push(Binary::zero());
        }
        btest /= 2;
    }

    guess.into_iter().rev().collect()
}

/// Runs the BKW algorithm with an extendion which uses the fast Walsh-Hadamardon Transform with the given [`LPNOracle`](crate::lpn_dist::LPNOracle) and returns with high probability the vector `s`.
/// If `a` or `n` are [`None`](Option), then they are filled in automatically according to the original specifications.
/// The input `t` is the tolerance between the major and minor vote for a given index for the algorithm to be satisfied.
pub fn wht_bkw(
    a: Option<usize>,
    n: Option<usize>,
    t: usize,
    o: impl LPNOracle,
) -> (DVector<Binary>, usize) {
    let k = o.dim();

    let da = max(1, (2.0 * (k as f64).log2()).ceil() as usize); // Suggested a value from paper, multiplied by a different constant for faster results given smaller k.
    let a = a.unwrap_or(da);
    let b = (k + a - 1) / a; // b = ceil(k/a)

    let dn: usize = max(1, (t + a) * 2usize.pow(b as u32) - a); // Usually there will be t vectors for each index at the end since t * 2^b vectors will make it to the end at least.
    let n = n.unwrap_or(dn);

    println!("k: {}", k);
    println!("a: {}", a);
    println!("b: {}", b);
    println!("n: {}", n);

    let mut o = LPNDataSaver::new(LPNOracleAdapter(a * b, o));

    // Use the reduce function to get all the indices.

    let mut guesses = vec![Binary::zero(); k];

    for i in 0..a {
        print!("\r\x1B[2KStep: {} / {}", i + 1, a);
        io::stdout().flush().unwrap();
        let mut indices = vec![];
        for j in i * b..i * b + b {
            if j < k {
                indices.push(j);
            }
        }
        o.reset_data_use();
        let partial_guesses = reduce(a, b, n, t, &mut o, &indices, i + 1);
        for (fi, ti) in indices.iter().enumerate() {
            guesses[*ti] = partial_guesses[fi];
        }
    }

    println!("\rBKW algorithm finished");

    (DVector::from(guesses), o.num_saved())
}

#[cfg(test)]
mod test {

    use crate::lpn_dist::LPNDist;

    use super::*;

    use rand::prelude::*;
    use rand_pcg::Pcg64;

    const ZE: Binary = Binary::new(false);
    const ON: Binary = Binary::new(true);

    const SEED: u64 = 0xEDAC_5FBE_96C1_B5AB;

    fn create_rng() -> impl Rng {
        Pcg64::seed_from_u64(SEED)
    }

    /// Tests that a specific example works as intended.
    #[test]
    fn sample_wht() {
        let case = vec![3, 2, -4, -4];
        let mut output = vec![0; 4];
        let (diff, ind) = walsh_hadamard_transform(case, &mut output);
        assert_eq!(output, vec![3, 1, 13, 1]);
        assert_eq!(diff, 10);
        assert_eq!(ind, 2);
    }

    /// Checks that the algorithm works given no error.
    /// It should work all of the time, even though it could technically run forever.
    #[test]
    fn no_error_wht_bkw() {
        let rng = create_rng();
        let s = DVector::from_vec(vec![
            ZE, ZE, ON, ON, ZE, ON, ON, ZE, ON, ZE, ON, ON, ZE, ON, ZE, ON, ON, ZE, ZE, ON, ON, ON,
            ON, ZE, ZE, ZE, ON,
        ]);
        let dist = LPNDist::new(s.clone(), rng, 0.0);
        let (sf, samples) = wht_bkw(Some(8), None, 10, dist);
        println!("Number of samples used: {}", samples);
        assert_eq!(s, sf);
    }
}
