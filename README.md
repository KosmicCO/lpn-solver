# LPN Solver

Find GitLab Repository [here](https://gitlab.com/KosmicCO/lpn-solver).

Learning Parity with Noise (LPN) is a problem from information theory which is usually relevant to machine learning which states that for some uniformly chosen vectors `A` and vector `s`, that the set of samples `sA` plus some vector `e` drawn from Bernoulli noise, it is hard to find `s`.

The algorithms covered in this project are the BKW algorithm, which solves this problem in sub-exponential time, and an extention which optimizes it.

Some relevant works are as follows

- (1) (Page 358 in the PDF) https://link.springer.com/content/pdf/10.1007%2F11832072.pdf
- (2) https://en.wikipedia.org/wiki/Hadamard_transform
- (3) https://eprint.iacr.org/2020/1467.pdf
- (Can't find it again at this address) https://wiki.epfl.ch/edicpublic/documents/Candidacy%20exam/PR13Bogos.pdf#page4
- https://arxiv.org/pdf/cs/0010022.pdf



## Learning Parity with Noise

In the Learning Parity with Noise (LPN) problem, one would like to find secret `s` of length `k` only from seeing vectors `a_i` and the resulting `<a_i, s> + e`, where `e` is some error drawn from Bernoulli noise with some rate `δ`.
From its inception, being able to solve the problem would allow for very efficient information transfer and error correcting; however, the problem is believed to be hard (i.e. not solvable by a classical/quantum computer in poly-time).
The LPN problem was formulated in the field of Information Theory; however, it has found uses in Cryptography and Machine learning.

In Cryptography, it is used in some primitives, especially Pseudorandom Generators, to make them resistant to attacks from quantum computers, since the LPN problem is also believed to be hard for quantum algorithms.
In Machine Learning, a direct result of the LPN problem would be the existance of an efficient linear learner, but because the problem is hard, such an algorithm is not believed to exist.
This fact has influenced the early works into the theoretical capabilities of a machine learning algorithm.

The LPN problem is defined formally as follows:
Let `k` be a positive integer, `s` be a binary vector of length `k`, and `A_s` be the distribution that returns `<a, s> + e`, where `a` is drawn uniformly from the `k`-bit vector space and `e` is `1` with some probability `δ`.
The goal is to find some algorithm `A` that has access to the distribution `A_s` and is able to find `s` with some probability.

## Trivial Algorithms

Since LPN is believed to be hard, any algorithm that is concieved is not going to be at least quantum polynomial time.
It can be trivially solved by a machine in NP.
In particular, we simply enumerate all possible `s` values (since they are of polynomial scaling length `k`) and see which one best fits the data.
If this algorithm was executed on a classical computer, it would have a runtime of `O(2^(2*k))` implemented in the naive way (not even covering memory usage) by taking every `s` and counting how many inputs `a` of the exponential `O(2^k)` samples are consistent with the given `s`.
This can actually be optimized using a generalized discrete Fourier Transform called the Walsh-Hadamard Transform (we will talk about it in depth later) to get a runtime of `O(k2^k)`.
Maybe that isn't as trivial, but we'll be to know it better later.
The trivial algorithms grow faster than the classic `2^k`, so they usually are not considered.

## Blum-Kalai-Wasserman Algorithm

The Blum-Kalai-Wasserman algorithm runs just under exponential time, in that it is technically `o(2^n)`.
In short, it takes a number of input samples `a_1..a_n` in a batch and tries to break them down into `a` blocks of size `b` bits and matches up vectors with the same `b` end bits so that you can end up with vectors of the form `e_i`.
Consider a demonstration (which I would have very much appreciated when reading others' papers):

Say that `a` is 2 and `b` is 2 for `k` equal to 4 with associated values.
```
                    v
a_1: 1 0 | 1 1      1
a_2: 0 0 | 1 0      0
...
a_n: 0 1 | 0 1      0
```

Lets say we want to find a guess for the first `b` bits of `s`, first we partition our list of `a_i` vectors into classes based on the last `b` bits:

```
                    v
c 0 0:
a_4: 0 1 | 0 0      1
a_5: 0 0 | 0 0      0
a_9: 1 1 | 0 0      0
...

c 0 1:
...

c 1 0:
...

c 1 1:
a_1: 1 0 | 1 1      1
a_7: 0 1 | 1 1      0
...
```

For the classes that are not simply all zeros, we can pick a random vector from the class, call it `a*`, and add it to the other vectors, including outputs.
In our example, the vectors in the `c 0 0` class would be unchanged, but in the `c 1 1` class, lets say that `a*` is `a_1`, then in the next round we would append the vector

```
                        v
a_1 + a_7: 1 1 | 0 0    1
```

Note that doing this transformation results in all the remaining vectors having their last `b` bits equal to zero AND maintain the property that the resulting vector is a valid output of the distribution given that the used vectors have an even number of errors [(1)](https://link.springer.com/content/pdf/10.1007%2F11832072.pdf):
```
(<a_1, s> + e_1) + (<a_2, s> + e_2) = <a_1 + a_2, s> + (e_1 + e_2) ~ <a_1 + a_2, s> + e
```

Thus, if this process is repeated `a` - 1 times, we will have a number of vectors with only `b` possibly non-zero first bits and the rest all zeros.
At this point, the algorithm then finds all the vectors of the form `e_i` and tallies what the output of each is [(1)](https://link.springer.com/content/pdf/10.1007%2F11832072.pdf).
Consider that is one of our samples was `e_i`, then we would have that
```
<e_i, s> + e = s_i + e
```
As such, if we get enough `e_i` vectors for each `i`, then we can see approximately what the `i`-th bit of `s` is.
Unfortunately, the algorithm can only discard the final vectors that are not in that form, and if there is an index that isn't accounted for, then the process needs to be repeated anew.

With this algorithm, a runtime of `O(2^(k/lg(k)))` can be achieved, which is definitely much better than `O(k2^k)` and `O(2^(2*k))`; however, note that no memory bounds are given.
When it comes to possibly exponential algorithms, memory is usually of the highest concern.

## Walsh-Hadamard Transform

According to the Wikipedia, the Walsh-Hadamard Transform (WHT) is a generalization of Fourier Transforms which is praised for its extremely light-weight computation requirement.
Prior to the quantum computer, the WHT was used to compress images by NASA and such [(2)](https://en.wikipedia.org/wiki/Hadamard_transform).

For our purposes, we wish to essentially count how many examples are consistent with some `b`-bit vector guess for part of the secret `s`.
If we count the number of 0 and 1 output bits associated with every `b`-bit vector by adding 1 for a 0 bit and -1 for a 1-bit, call this value `X_k` for `k` as the bit vector as a number, then we find the "count" for a number/bit-vector guess `w` with the formula [(3)](https://eprint.iacr.org/2020/1467.pdf)
```
X_w = ∑_k X_k (-1)^(w·k)
```
Note that the formula only really uses negation and addition, not even any multiplication! (the exponent is the dot product between the number as bit-vectors).
After doing this, the most-likely guess is the `w` which satisfies
```
max_w |X_w|
```

The algorithm was also made faster in a version called fast WHT (fWHT).
Truthfully, I don't really fully grasp why the fast algorithm works or even why the original WHT computes this for us; however, given that I haven't studied quamtum computation (yet), I will leave it for a future self to find out.

In short, the fast algorithm runs in time `O(n*lg n)` for a vector input of length `n`.

## BKW with the WHT Extention

The original BKW algorithm doesn't really have a way to use the non-singular vectors, which is a waste.
Instead of only selecting the singular vectors from the remaining, we can run a smaller version of the trivial algorithm on the vectors with only `b` bits [(3)](https://eprint.iacr.org/2020/1467.pdf).
Of course, if the first trivial algorith was used, this step would have a runtime of `O(2^(2b))`, which certainly defeats the purpose of having it; however, if we use the fWHT described previously, we can get a runtime of `O(b2^b)`, which is in line which the runtime of the rest of the algorithm.
In general, this method is able to use the data more effectively.

## Small Change

The implemented algorithms are slightly modified from the ones in the paper.
Because the ones in the papers are not particularly concerned with how one would actually implement the algorithms (e.g. fixing the number of samples in advanced), the implementations in this library are based more on example counting.
For example, the implementation of the BKW algorithm here stops processing batches when the number of guesses towards a one instead of a zero bit for a given index surpasses some tolerance.
I find that this makes it easier to asymptotically tune the accuracy, since it has a built-in meaning (i.e. the larger the tolerance, the lower the chance that a bunch of errors came in and were processed to make the wrong bit artificially high).
The caviate to using this method is that the original mathematics/predictions for the accuracy of the algorithm doesn't work exactly.
That being said, the works do not really consider cases outside of the specific parameters of `a` equal to `0.5 lg k`, etc.
The original parameters, though optimal, use many more samples per batch exponentially, so it is not as great for my computer.
As such, I tend to set `a` such that `b` is 4 or so.
Even though this technically would not scale as well, for the small length of `s` for which the algorithm is feasible, it is fine.

## Testing

We run a few tests with larger `a` values (decreasing memory use significantly) to compare the regular BKW algorithm with the BKW algorithm with the fWHT.
We run these tests on the release version of the code so that it runs the fastest (i.e. optimized rust code).
We will run the code on `k` of sizes 32, 64, and 96 with error 0.01 for each and threshholds 1, 10, and 100 (which is what is required to get correct results most of the time).
`a` will be chosen so that `b` is 8.

The tests will be run with the following command:

`cargo run --release -- -s <k> -a <a> -e 0.01 -t <t> [-x]`

Note that these commands only work with cargo installed, withough cargo, the following can be run:

`./random_s -s <k> -a <a> -e 0.01 -t <t>`

### Timing Tests

| Alg: `k` | Time | Samples | Incorrect Bits |
| :-- | --: | --: | --: |
| BKW: 32 | 9ms | 2552 | 0 |
| BKW: 64 | 1459ms | 101200 | 1 |
| BKW: 96 | 1662251ms| 45970640 | 13 | 
| WHT: 32 | 5ms | 1276 | 0 |
| WHT: 64 | 119ms | 4600 | 0 |
| WHT: 96 | 8334ms| 229280| 47 |
| WHT: 96 `a=8` | 29974ms |  442360 | 0 |

### Results

We first note that the traditional algorithm takes forever to run and takes way more samples.
Because the samples are re-used between tests, the samples used is related to the max number of samples used by a pass-through.
We see that the WHT algorithm does a much better job of using fewer samples, 100x fewer samples towards the end of the tests.
Moreover, the BKW algorithm's times grow so quickly as to make it difficult to run tests on because it would take too long otherwise.

We also note that the number of incorrect bits for the WHT on `k` 96 and `a` 12 has the expected number of wrong bits as if we simply guessed each bit from random.
The reason the algorithms are designed with `a` being so low is because there is a delicate tradeoff between the number of samples that are required to form a singleton vector and the exponential number of vector `b` classes we need.
In other words, needing exponentially more classes makes the algorithm slower simply because exponentially more elements are required, but having smaller classes and more iterations means that each singleton vector will have more and more vectors it needs to rely on being correct, or else the error rate might be so high that it needs to have a very high tolerance `t`.

In general though, if `t` doesnt change, then making `a` smaller makes the algorithm slower much more quickly than increasing `t`; however, the probability of failure decreases much faster as `a` drops.
For the BKW algorithm with `k` being 96, we see that it is not really that feasible to drop `a` even lower, since it might quadruple the already 20 mins it takes to run it; however, the WHT algorithm is able to correctly guess the entire vector with only double the time lowering `a` from 12 to 8, while still taking 100x less time than the original BKW algorithm.

It's worth noting that for any cryptographic purposes, usually `k` is set to 256 if not 512, since the problem quickly becomes intractible with so many dimensions.
From my preliminary testing, both algorithms failed to recover better than random guessing the secret when `k` is 128 on my laptop.
Ofcourse, a govt. agency with much more computational ability could probably break it for `k` 128, which is why it isn't used.

## Conclusion

The BKW algorithm and its WHT derived extention are very useful in demonstrating in practice how stochastic information relayed by a seemingly random process can still be somewhat reasonably extracted from data.
Furthermore, the LPN problem can also be made more specific to be more useful for coding theory directly, with protocols such as error-correcting codes which multiply an input "secret" by a series of carefully designed `a_i` vectors such that the solutions to the linear equations can, if not be efficiently solved, have upper bounds on the number needed to be sent to reconstruct a message.

In the opposite view, this work can also be seen as a generalization of that coding theory in which we know what the vectors are, but completely forgot what the method was for recombining the vectors; in which case the only option is to brute-force it with this algorithm.